import os
import sys



def fix_indents(lines, recursions):
    counts = []
    for index,line in enumerate(lines):
        if line.lower().find("toyaml") != -1:
            continue 
        if (line.find("{{ end") != -1 or line.find("{{- end") != -1 or line.find('else ') != -1):
            if lines[index-1].lstrip()[:2] == "{{" and lines[index-1].find("{{ end") == -1 and lines[index-1].find("{{- end") == -1:
                lines[index-1] = lines[index-1].lstrip().rjust(len(lines[index-1].lstrip())+counts[-1]+1)
            string = line.lstrip().rjust(len(line.lstrip())+counts.pop())
            lines[index] = string
        if line.lstrip()[:2] == "{{" and line.find("{{ end") == -1 and line.find("{{- end") == -1:
            count = len(lines[index+1])-len(lines[index+1].lstrip(' '))
            if line.find('if ') != -1 or line.find('range ') != -1 or line.find('with ') != -1:
                counts.append(count)
            string = line.lstrip().rjust(len(line.lstrip())+count)
            lines[index] = string
    if recursions >= 2:
        return lines
    else:
        recursions+=1
        return fix_indents(lines, recursions+1)        



def main(path=os.getcwd()):
    os.system("mkdir fixed")
    for file in os.listdir(path):
        if file.endswith((".yml",".yaml")):
            f = open(file, 'r')
            lines = f.readlines()
            with open ('fixed/'+file, 'w') as f:
                f.write(''.join(fix_indents(lines, 1)))


if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()
